//package com.example.entity;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.Table;
//import javax.validation.constraints.Min;
//import javax.validation.constraints.NotNull;
//import javax.validation.constraints.Size;
//
////如果類別名稱與資料表名稱不一致，就可以使用@Table來指定對應的資料表名稱
//@Entity
//@Table(name="product")
//public class Product {
//	
//	@Id
//	@GeneratedValue(strategy=GenerationType.AUTO)
//	private long id;
//	
//	@NotNull
//	@Size(min = 1)
//	private String name;
//
//	private String type;
//	
////	如果欄位名稱不一致，就可以使用@Column來指定對應的欄位名稱
//	@Column(name="inventory")
//	@Min(1)
//	private int inventory;
//	
//	@Column(name="safestock")
//	@Min(1)
//	private int safeStock;
//	
//	private String description;
//	
//	public Product(){	
//	}
//	
//	public Product(String name, String type, int inventory, int safeStock){
//		this.name = name;
//		this.type = type;
//		this.inventory = inventory;
//		this.safeStock = safeStock;
//	}
//	
//	public Long getId() {
//		return id;
//	}
//	
//	public void setId(Long id) {
//		this.id = id;
//	}
//
//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	public String getType() {
//		return type;
//	}
//
//	public void setType(String type) {
//		this.type = type;
//	}
//	
//	public int getInventory() {
//		return inventory;
//	}
//
//	public void setInventory(int inventory) {
//		this.inventory = inventory;
//	}
//
//	public int getSafeStock() {
//		return safeStock;
//	}
//
//	public void setSafeStock(int safeStock) {
//		this.safeStock = safeStock;
//	}
//	
//	public String getDescription() {
//		return description;
//	}
//	
//	public void setDescription(String description) {
//		this.description = description;
//	}
//}
