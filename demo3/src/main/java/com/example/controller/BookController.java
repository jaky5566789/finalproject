package com.example.controller;

import java.sql.SQLException;

import org.aspectj.weaver.Iterators.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.dao.BookCategoryDAO;
import com.example.dao.BookDAO;
import com.example.entity.Book;
import com.example.entity.BookCategory;
import com.example.entity.ShoppingCart;

import javax.servlet.http.HttpSession;
@Controller
public class BookController {
	
	@Autowired
	BookDAO dao;

	@Autowired
	BookCategoryDAO categoryDao;

	@Autowired
	 ShoppingCart cart;

//	新增
	@RequestMapping(value = "/bookCreate", method = RequestMethod.GET)
	public ModelAndView openFormCreate() {
		ModelAndView model = new ModelAndView("bookCreate");
		Iterable<BookCategory> categories = categoryDao.findAll();
		model.addObject("allBookCategories", categories);
		return model;
	}
	@RequestMapping(value = "/bookCreate", method = RequestMethod.POST)
	public ModelAndView processFormCreate(Book book) {
		ModelAndView model = new ModelAndView("redirect:/bookRetrieveAll");
		dao.save(book);
		model.addObject(book);
		return model;
	}
	
//	讀取
	@RequestMapping(value = {"/bookRetrieveAll","/book"}, method = RequestMethod.GET)
	public ModelAndView retrieveBooks() {
		ModelAndView model = new ModelAndView("bookList");
//		類別下拉式選單
		Iterable<BookCategory> categories = categoryDao.findAll();
		model.addObject("allBookCategories",categories);
//		table裡每本書的類別
		BookCategory category = categories.iterator().next();
		model.addObject("bookCategory",category);
//		table裡每本書
		Iterable<Book> allBooks = dao.findAll();
		model.addObject("allBooks",allBooks);
		return model;
	}
//	讀取，依類別查詢
	@RequestMapping(value = "/bookRetrieveByCategory", method = RequestMethod.POST)
	public ModelAndView retrieveBooksByCategory(@RequestParam(value = "id", required = false, defaultValue = "1") Long id) {
		ModelAndView model = new ModelAndView("bookList");
//		先找出所有書類別
		Iterable<BookCategory> categories = categoryDao.findAll();
		model.addObject("allBookCategories", categories);
//		使用者查詢的書類別，以findOne方法找到，指派給型態為BookCategory的變數category
		BookCategory category = categoryDao.findOne(id);
//		table裡使用者要找的類別
		model.addObject("bookCategory", category);
		model.addObject("allBooks", category.getBooks());
		return model;
	}

//	更新
	@RequestMapping(value = "/bookUpdate", method = RequestMethod.GET)
	public ModelAndView openFormUpdate(@RequestParam(value = "id", required = false, defaultValue = "1") Long id) {
		ModelAndView model = new ModelAndView("bookUpdate");
		Book book = dao.findOne(id);
		model.addObject(book);
		Iterable<BookCategory> categories = categoryDao.findAll();
		model.addObject("allBookCategories", categories);
		return model;
	}
	@RequestMapping(value = "/bookUpdate", method = RequestMethod.POST)
	public ModelAndView processFormUpdate(@ModelAttribute Book book) throws SQLException {
		ModelAndView model = new ModelAndView("redirect:/bookRetrieveAll");
		dao.save(book);
		return model;
	}
	
//	刪除
	@RequestMapping(value = "/bookDelete", method = RequestMethod.GET)
	public ModelAndView deleteBook(@RequestParam(value = "id", required = false, defaultValue = "1") Long id) {
		ModelAndView model = new ModelAndView("redirect:/bookRetrieveAll");
		dao.delete(id);
		return model;
	}
	 @RequestMapping(value = "/", method = RequestMethod.GET)
	    public ModelAndView openFormLogin() {
	       ModelAndView model = new ModelAndView("hi");
	   
	       return model;
	    }

	    @RequestMapping(value = "/HelloWithId", method = RequestMethod.POST)
	    public ModelAndView processForm(@ModelAttribute("id") String id, HttpSession session) {
	       ModelAndView model = new ModelAndView("redirect:/bookRetrieveAll");
	       session.setAttribute("loginId", id);
	       return model;
	    }
	    @RequestMapping(value = "/shoppingCartAdd", method = RequestMethod.GET)
	    public ModelAndView addShoppingCart(
	      @RequestParam(value = "id", required = false, defaultValue = "1") Long id) {
	     ModelAndView model = new ModelAndView("redirect:/bookRetrieveAll");
	     Book book = dao.findOne(id);
	     cart.add(book);

	     return model;
	    }

	    @RequestMapping(value = "/shoppingCartList", method = RequestMethod.GET)
	    public ModelAndView showShoppingCart() {
	     ModelAndView model = new ModelAndView("shoppingCart");
	     return model;
	    }

	    @RequestMapping(value = "/cleanShoppingCart", method = RequestMethod.GET)
	    public ModelAndView cleanShoppingCart() {
	     ModelAndView model = new ModelAndView("shoppingCart");
	     cart.cleanup();
	     return model;
	    }
	    
	   }


