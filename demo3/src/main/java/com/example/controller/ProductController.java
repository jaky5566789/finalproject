//package com.example.controller;
//
//import java.sql.SQLException;
//
//import javax.validation.Valid;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.validation.BindingResult;
//import org.springframework.web.bind.annotation.ModelAttribute;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.servlet.ModelAndView;
//
//import com.example.dao.ProductDAO;
//import com.example.entity.Product;
//
//@Controller
//public class ProductController {
//	@Autowired
//	ProductDAO dao;
//
//	@RequestMapping(value = "/productCreate", method = RequestMethod.GET)
//	public ModelAndView openForm(Product product) {
//		ModelAndView model = new ModelAndView("productCreate");
//		model.addObject(product);
//		return model;
//	}
//
////	新增
//	@RequestMapping(value = "/productCreate", method = RequestMethod.POST)
//    public ModelAndView processFormCreate(@Valid @ModelAttribute Product product,  BindingResult bindingResult) throws SQLException {
//       ModelAndView model = new ModelAndView("redirect:/productRetrieveAll");
//       if (bindingResult.hasErrors()) {
//         model = new ModelAndView("productCreate");
//         return model;
//       }
//       dao.save(product);
////       model.addObject("name", product.getName());
////       model.addObject("type", product.getType());
////       model.addObject("inventory", product.getInventory());
////       model.addObject("safeStorage", product.getSafeStock());
//       model.addObject(product);
//       return model;
//    }
//	
////  讀取
//	@RequestMapping(value = {"/productRetrieveAll","/"}, method = RequestMethod.GET)
//	public ModelAndView retrieveProducts() throws SQLException {
//		Iterable<Product> product = dao.findAll();
//		ModelAndView model = new ModelAndView("productList");
//		model.addObject("product",product);
//		return model;
//	}
//	
////	更新
//	@RequestMapping(value = "/productUpdate", method = RequestMethod.GET)
//    public ModelAndView openFormUpdate(@RequestParam(value="id", required=false, defaultValue="1") Long id) {
//       ModelAndView model = new ModelAndView("productUpdate");
//       Product product = dao.findOne(id);
//       model.addObject(product);
//       return model;
//    }
//	@RequestMapping(value = "/productUpdate", method = RequestMethod.POST)
//    public ModelAndView processFormUpdate(@ModelAttribute Product product) throws SQLException {
//       ModelAndView model = new ModelAndView("redirect:/productRetrieveAll");
//       dao.save(product);             
//       return model;
//    }
//	
////	刪除
//	@RequestMapping(value = "/productDelete", method = RequestMethod.GET)
//    public ModelAndView deleteProduct(@RequestParam(value="id", required=false, defaultValue="1") Long id) {
//       ModelAndView model = new ModelAndView("redirect:/productRetrieveAll");
//       dao.delete(id);
//       return model;
//    }
//	
//	
//}
