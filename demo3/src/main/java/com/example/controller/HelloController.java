//package com.example.controller;
//
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.ModelAttribute;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.servlet.ModelAndView;
//
//import com.example.entity.Product;
//
//@Controller
//public class HelloController {
//
//	@RequestMapping("/hello")
//	public String home(Model model) {
//		model.addAttribute("name", "ben");
//		return "hi";
//	}
//
//	@RequestMapping(value={"/helloWithId","/"}, method = RequestMethod.POST)
//	public ModelAndView home(@ModelAttribute("loginId") String name, Product product) {
//		ModelAndView model = new ModelAndView("hello");
//		if(name.equals("sa")){
//			model = new ModelAndView("productCreate");
//			return model;
//		}
//		model.addObject("name", name);
//		return model;
//	}
//
//}
