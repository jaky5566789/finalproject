package com.example.dao;

import org.springframework.data.repository.CrudRepository;

import com.example.entity.BookCategory;

public interface BookCategoryDAO extends CrudRepository<BookCategory, Long> {
	
	

}
