//package com.example.dao;
//
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.CrudRepository;
//
//import com.example.entity.Product;
//
//public interface ProductDAO extends CrudRepository<Product, Long> {
//
//	//public Product findById(Long id);
////		public Iterable<Product> findByOrderByIdAsc();
//		public Iterable<Product> findByIdGreaterThan(Integer id);
//		
//		//in @Query, the entity name is case sensitive
//		@Query("select c from Product c")
//		public Iterable<Product> findOverId();
//	
//}
